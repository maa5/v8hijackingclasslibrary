﻿using System.Web.Mvc;
using Umbraco.Core.Logging;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;


namespace routehijacking.Core.Controllers
{
    public class HomePageController : RenderMvcController
    {
        public ActionResult Index(ContentModel model)
        {
            Logger.Info<HomePageController>($"{model.Content.Id} value of sugar is {Request.Params["sugar"]}");
            return CurrentTemplate(model);

        }

    }

}
